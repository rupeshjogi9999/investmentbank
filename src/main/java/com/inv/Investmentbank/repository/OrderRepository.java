package com.inv.Investmentbank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inv.Investmentbank.entities.OrderTransaction;
import com.sun.xml.bind.v2.model.core.ID;

public interface OrderRepository extends JpaRepository<OrderTransaction, ID> {

}
