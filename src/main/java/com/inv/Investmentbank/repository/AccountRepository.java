package com.inv.Investmentbank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inv.Investmentbank.entities.InvestmentAccount;
import com.sun.xml.bind.v2.model.core.ID;

public interface AccountRepository extends JpaRepository<InvestmentAccount, ID> {
	
	public InvestmentAccount findByAccountNumber(long accountNumber);

}
