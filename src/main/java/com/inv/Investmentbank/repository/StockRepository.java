package com.inv.Investmentbank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inv.Investmentbank.entities.StockDetails;
import com.sun.xml.bind.v2.model.core.ID;

public interface StockRepository extends JpaRepository<StockDetails, ID> {

	public StockDetails findByStockName(String stockName);
}
