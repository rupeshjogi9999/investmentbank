package com.inv.Investmentbank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvestmentbankApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvestmentbankApplication.class, args);
	}

}
