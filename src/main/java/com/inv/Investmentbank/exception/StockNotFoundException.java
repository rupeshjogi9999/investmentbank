package com.inv.Investmentbank.exception;

public class StockNotFoundException extends Exception{

	
private static final long serialVersionUID = 1L;
	
	/*
	 * AccountNotFoundException(){ super(); }
	 */
	
	public StockNotFoundException(String message){
		super(message);
	}
}
