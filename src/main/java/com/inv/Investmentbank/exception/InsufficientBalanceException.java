package com.inv.Investmentbank.exception;

public class InsufficientBalanceException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/*
	 * AccountNotFoundException(){ super(); }
	 */
	
	public InsufficientBalanceException(String message){
		super(message);
	}
}
