package com.inv.Investmentbank.exception;

public class AccountNotFoundException extends Exception {

	
   private static final long serialVersionUID = 1L;
	
	/*
	 * AccountNotFoundException(){ super(); }
	 */
	
	public AccountNotFoundException(String message){
		super(message);
	}
}
