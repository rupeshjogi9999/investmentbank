package com.inv.Investmentbank.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inv.Investmentbank.dto.OrderRequestDto;
import com.inv.Investmentbank.dto.OrderResponseDto;
import com.inv.Investmentbank.exception.AccountNotFoundException;
import com.inv.Investmentbank.exception.InsufficientBalanceException;
import com.inv.Investmentbank.exception.StockNotFoundException;
import com.inv.Investmentbank.service.OrderService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	OrderService orderService;
	
	@PostMapping("/order")
	public ResponseEntity<OrderResponseDto> order(@RequestBody OrderRequestDto orderRequestDto) throws AccountNotFoundException, InsufficientBalanceException, StockNotFoundException {
		//orderService.placeOrder(orderRequestDto);
		return new ResponseEntity<OrderResponseDto>(orderService.placeOrder(orderRequestDto), HttpStatus.CREATED);
	}
}
