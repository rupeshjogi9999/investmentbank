package com.inv.Investmentbank.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name="stock_details")
@Component
public class StockDetails {

	@Id
	@Column(name="stockid")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long stockId;
	
	@Column(name="date")
	private LocalDateTime date;
	
	@Column(name="stockdesc")
	private String stockDes;
	
	@Column(name="stockname")
	private String stockName;
	
	@Column(name="stockprice")
	private double stockPrice;

	public StockDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public long getStockId() {
		return stockId;
	}

	public void setStockId(long stockId) {
		this.stockId = stockId;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getStockDes() {
		return stockDes;
	}

	public void setStockDes(String stockDes) {
		this.stockDes = stockDes;
	}

	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	public double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}
	
}
