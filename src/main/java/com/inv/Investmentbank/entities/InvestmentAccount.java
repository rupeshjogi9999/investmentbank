package com.inv.Investmentbank.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name="investment_account")
@Component
public class InvestmentAccount {

	@Id
	@Column(name="accountnumber")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long accountNumber;
	
	@Column(name="accountbalance")
	private double accountBalance;
	
	@Column(name="date")
	private LocalDateTime date;
	
	@Column(name="accounttype")
	private String accountType;
	
	@Column(name="customerid")
	private long customerId;
	
	public InvestmentAccount() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	
	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

}
