package com.inv.Investmentbank.service;

import com.inv.Investmentbank.dto.OrderRequestDto;
import com.inv.Investmentbank.dto.OrderResponseDto;
import com.inv.Investmentbank.exception.AccountNotFoundException;
import com.inv.Investmentbank.exception.InsufficientBalanceException;
import com.inv.Investmentbank.exception.StockNotFoundException;

public interface OrderService {

	public OrderResponseDto placeOrder(OrderRequestDto orderRequestDto) throws AccountNotFoundException, InsufficientBalanceException, StockNotFoundException;
	
}
