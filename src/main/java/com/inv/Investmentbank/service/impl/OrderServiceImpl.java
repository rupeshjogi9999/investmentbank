package com.inv.Investmentbank.service.impl;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inv.Investmentbank.dto.OrderRequestDto;
import com.inv.Investmentbank.dto.OrderResponseDto;
import com.inv.Investmentbank.entities.InvestmentAccount;
import com.inv.Investmentbank.entities.OrderTransaction;
import com.inv.Investmentbank.entities.StockDetails;
import com.inv.Investmentbank.exception.AccountNotFoundException;
import com.inv.Investmentbank.exception.InsufficientBalanceException;
import com.inv.Investmentbank.exception.StockNotFoundException;
import com.inv.Investmentbank.repository.AccountRepository;
import com.inv.Investmentbank.repository.OrderRepository;
import com.inv.Investmentbank.repository.StockRepository;
import com.inv.Investmentbank.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	InvestmentAccount account = new InvestmentAccount();
	StockDetails  stockdetails = new StockDetails();
	
	@Autowired
	StockRepository stockRepository;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public OrderResponseDto placeOrder(OrderRequestDto orderRequestDto) throws AccountNotFoundException, InsufficientBalanceException, StockNotFoundException {
		 stockdetails= stockRepository.findByStockName(orderRequestDto.getStockName());
		 account = accountRepository.findByAccountNumber(orderRequestDto.getAccountNumber());
		 OrderResponseDto orderResponseDto = new OrderResponseDto();
		 int totalCost = orderRequestDto.getQuantity()*orderRequestDto.getUnitPrice();
		
		  if(stockdetails.getStockName() != orderRequestDto.getStockName()) { 
			  throw new StockNotFoundException("Stock Not Found"); 
			  
		  }
		 
		
		if(totalCost>account.getAccountBalance())
		{
			throw new InsufficientBalanceException("Insufficient");
		}
		
	    if(account.getAccountBalance()!=orderRequestDto.getAccountNumber())
	    {
	    	throw new AccountNotFoundException("Account Not found");
	    }else 
	    {
		OrderTransaction orderTransaction = new OrderTransaction();
		orderTransaction.setAccountNumber(orderRequestDto.getAccountNumber());
		orderTransaction.setDate(LocalDateTime.now());
		orderTransaction.setInstrumentType(orderRequestDto.getInstrumentType());
		orderTransaction.setQuantity(orderRequestDto.getQuantity());
		orderTransaction.setUnitPrice(orderRequestDto.getUnitPrice());
		orderRepository.save(orderTransaction);
		orderResponseDto.setStatusCode(200);
		orderResponseDto.setMessage("Order Placed successfully!");
		return orderResponseDto;
	    
	    }
	}

}
